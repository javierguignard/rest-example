from django.conf.urls import url, include
from app_users import views
from rest_framework.routers import DefaultRouter

# Create a router and register our viewsets with it.
router = DefaultRouter(trailing_slash=False)
router.register(r'profiles', views.ProfileViewSet)
router.register(r'employees', views.EmployeeViewSet)
router.register(r'visits', views.VisitViewSet)
router.register(r'drugs', views.DrugViewSet)
#router.register(r'testing', views.Testing.as_view())



# The API URLs are now determined automatically by the router.
# Additionally, we include the login URLs for the browsable API.
urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'testing', views.Testing.as_view()), #EMSER: una vista basada en clases, pero que no pertenece a un dominio en particular.
    url(r'photo', views.returnPhoto) #EMSER: una vista basada en un modelo (igual al resto de las vistas)
]
